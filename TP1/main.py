import math
import random
import numpy as np
import matplotlib.pyplot as plt

# Nombre de scorpion généré
n = 1200

# Probabilité que tous les gènes d'un scorpion et de sa flèche mute
mutation_genes_probabilite = 10/100

# Nombre de génération maximum
generation = 10


class Scorpion:
    # création des gènes du scorpion
    def __init__(self, longueur_bras, base_bras, hauteur_bras, longueur_corde, longueur_fleche, base_fleche, hauteur_fleche, masse_volumique, module_young, coef_poisson, gravite, degres_angle):
        self.longueur_bras = longueur_bras
        self.base_bras = base_bras
        self.hauteur_bras = hauteur_bras
        self.longueur_corde = longueur_corde
        self.longueur_fleche = longueur_fleche
        self.base_fleche = base_fleche
        self.hauteur_fleche = hauteur_fleche
        self.masse_volumique = masse_volumique
        self.module_young = module_young
        self.coef_poisson = coef_poisson
        self.gravite = gravite
        self.degres_angle = degres_angle
        self.fitness = 0

    # Calcul nécessaire pour connaitre la distance parcouru par la flèche

    def ressort(self):
        return (1/3) * (self.module_young / (1 - 2 * self.coef_poisson))

    def longueur_vide(self):
        if((self.longueur_bras ** 2 - self.longueur_corde ** 2) < 0):
            return 0
        return 1/2 * math.sqrt(self.longueur_bras ** 2 - self.longueur_corde ** 2)

    def longueur_deplacement(self):
        return self.longueur_fleche - self.longueur_vide()

    def masse_projectile(self):
        return self.masse_volumique * self.base_fleche * self.hauteur_fleche * self.longueur_fleche

    def velocite(self):
        if(self.masse_projectile() == 0):
            return 0
        return math.sqrt((self.ressort() * self.longueur_deplacement() ** 2) / self.masse_projectile())

    def portee(self):
        return ((self.velocite() ** 2) / self.gravite) * math.sin(2 * math.radians(self.degres_angle))

    def energie_impact(self):
        return self.masse_volumique * self.base_fleche * self.hauteur_fleche * self.longueur_fleche

    def energie_joule(self):
        return (1 / 2) * self.masse_projectile() * self.velocite() ** 2

    def equivalence_joule(self):
        return self.energie_joule() / 4184

    # Limites :

    def moment_quadratique(self):
        I = (self.base_bras * self.hauteur_bras ** 3)/12
        return I

    def force_traction(self):
        return self.ressort() * self.longueur_deplacement()

    def fleche_f_max(self):
        if((48 * self.module_young * self.moment_quadratique()) == 0):
            return 0
        return (self.force_traction()*self.longueur_bras ** 3) / (48 * self.module_young * self.moment_quadratique())

    def rupture(self):
        return self.longueur_deplacement() > self.fleche_f_max()

    def tir(self):
        return self.longueur_vide() < self.longueur_fleche and self.longueur_corde < self.longueur_bras and not self.rupture()


class Materiaux_fleche:
    # Création des gènes de la flèche
    def __init__(self, nom, masse_volumique, module_young, coef_poisson):
        self.nom = nom
        self.masse_volumique = masse_volumique
        self.module_young = module_young
        self.coef_poisson = coef_poisson


# Matériraux utilisable pour la flèche avec la valeur de chacun de leurs gènes.
choix_materiaux = [Materiaux_fleche('acier', 7850, 210, 0.27),
                   Materiaux_fleche('aluminium', 2700, 62, 0.29),
                   Materiaux_fleche('béton', 2350, 30, 0.20),
                   Materiaux_fleche('cuivre', 8920, 128, 0.33),
                   Materiaux_fleche('fer', 7860, 208, 0.25),
                   Materiaux_fleche('laiton', 8470, 90, 0.37),
                   Materiaux_fleche('magnésium', 1740, 44, 0.35),
                   Materiaux_fleche('nickel', 8900, 207, 0.31),
                   Materiaux_fleche('or', 18900, 78, 0.42),
                   Materiaux_fleche('plomb', 11300, 15, 0.44),
                   Materiaux_fleche('titane', 4500, 114, 0.34)]


population = []


def generation_population():
    # Génération aléatoire pour chaque scorpion de leur valeur et la flèche utilisé
    for i in range(n):
        materiau_utilise = random.choice(choix_materiaux)
        population.append(Scorpion(random.randrange(0, 100, 1),
                                   random.randrange(0, 100, 1),
                                   random.randrange(0, 100, 1),
                                   random.randrange(0, 100, 1),
                                   random.randrange(0, 100, 1),
                                   random.randrange(0, 100, 1)/100,
                                   random.randrange(0, 100, 1)/100,
                                   materiau_utilise.masse_volumique,
                                   materiau_utilise.module_young,
                                   materiau_utilise.coef_poisson,
                                   9.81,
                                   random.randrange(0, 90, 1),))
    return random.choice(choix_materiaux)


def fitness():
    # calcul de la distance parcouru par la flèche
    for pop in population:
        if(pop.tir() == True):
            pop.fitness = pop.energie_impact()*pop.portee()*2
        else:
            pop.fitness = 0


def supression_population():
    # Suppression de la moitié des Scorpion qui ont tiré le moins loins.
    population.sort(key=lambda x: x.fitness, reverse=True)
    for x in range(round(n/2), n-1):
        del population[round(n/2)]


def croisement_genes(scorpion1, scorpion2):
    # Fusion des gènes d'un scorpion avec un autre. Mixe entre sa flèche et le scorpion.
    return Scorpion(scorpion1.longueur_bras,
                    scorpion1.base_bras,
                    scorpion1.hauteur_bras,
                    scorpion1.longueur_corde,
                    scorpion2.longueur_fleche,
                    scorpion2.base_fleche,
                    scorpion2.hauteur_fleche,
                    scorpion2.masse_volumique,
                    scorpion2.module_young,
                    scorpion2.coef_poisson,
                    scorpion2.gravite,
                    scorpion1.degres_angle)


def creation_nouvelle_population():
    # Création d'une nouvelle population en croisant des gènes.
    probs = [i.fitness for i in population]
    probs /= np.sum(probs)
    nouvelle_population = []
    for pop in population:
        scorpion2 = np.random.choice(population, p=probs)
        bebe_scorpion = croisement_genes(pop, scorpion2)
        nouvelle_population.append(bebe_scorpion)
        while True:
            scorpion3 = np.random.choice(population, p=probs)
            if(scorpion3.fitness != scorpion2.fitness):
                bebe_scorpion2 = croisement_genes(pop, scorpion3)
                if(bebe_scorpion2 not in nouvelle_population):
                    nouvelle_population.append(bebe_scorpion2)
                    break

    return nouvelle_population


def proba(probabilite):
    # Probabilité qu'un scorpion mute au lieu de se croiser avec un autre.
    return random.random() < probabilite


def mutation():
    # Change tous ses gènes aléatoirement.
    for i in range(0, len(population)-1):
        mutation_materiaux = random.choice(choix_materiaux)
        if(proba(mutation_genes_probabilite)):
            population[i].longueur_bras == random.randrange(0, 100, 1)
            population[i].base_bras == random.randrange(0, 100, 1)
            population[i].hauteur_bras == random.randrange(0, 100, 1)
            population[i].longueur_corde == random.randrange(0, 100, 1)
            population[i].longueur_fleche == random.randrange(0, 100, 1)
            population[i].base_fleche == random.randrange(0, 100, 1)/100
            population[i].hauteur_fleche == random.randrange(0, 100, 1)/100
            population[i].masse_volumique == mutation_materiaux.masse_volumique
            population[i].module_young == mutation_materiaux.module_young
            population[i].coef_poisson == mutation_materiaux.coef_poisson
            population[i].degres_angle == random.randrange(0, 90, 1)


def max_fitness():
    # Distance maximale parcouru par toutes les flèches propulsé par le scorpion.
    fitnesses = [i.fitness for i in population]
    return np.amax(fitnesses)


liste_max_fitness = []


def list_max_fitness():
    # Permet d'afficher la courbe de progression de la distance maximale.
    liste_max_fitness.append(max_fitness())
    return liste_max_fitness


def min_fitness():
    # Distance minimale parcouru par toutes les flèches propulsé par le scorpion.
    fitnesses = [i.fitness for i in population]
    filter_0 = list(filter((0).__ne__, fitnesses))
    return np.amin(filter_0)


liste_min_fitness = []


def list_min_fitness():
    # Permet d'afficher la courbe de progression de la distance minimale.
    liste_min_fitness.append(min_fitness())
    return liste_min_fitness


def moyenne_fitness():
    # Distance moyenne parcouru par toutes les flèches propulsé par le scorpion.
    fitnesses = [i.fitness for i in population]
    filter_0 = list(filter((0).__ne__, fitnesses))
    return np.mean(filter_0)


liste_moyenne_fitness = []


def list_moyenne_fitness():
    # Permet d'afficher la courbe de progression de la distance maximale.
    liste_moyenne_fitness.append(moyenne_fitness())
    return liste_moyenne_fitness


def afficher_population():
    # Visualisation rapide de l'avolution en fonction du nombre de génération.
    print('Distance maximum : ' + str(max_fitness()) + ' / Distance minimale : ' + str(min_fitness()) + ' / Distance moyenne : ' + str(moyenne_fitness()))


liste_generation = []


def start():
    global population
    generation_population()
    for i in range(0, generation):
        fitness()
        supression_population()
        population = creation_nouvelle_population()
        fitness()
        liste_generation.append(i+1)
        list_max_fitness()
        list_min_fitness()
        list_moyenne_fitness()
        mutation()
        afficher_population()

    plt.title("Evolution de la distance moyenne, maximale et minimale")
    plt.plot(liste_generation, liste_max_fitness, label="Valeur maximale")
    plt.plot(liste_generation, liste_min_fitness, label="Valeur minimale")
    plt.plot(liste_generation, liste_moyenne_fitness, label="Valeur moyenne")
    plt.xlabel('Générations')
    plt.ylabel('Distance en mètre')
    plt.legend()
    plt.show()


start()
