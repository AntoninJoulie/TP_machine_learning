import random as rand
import networkx as nx
import matplotlib.pyplot as plt
from numpy.random import choice
import time
import numpy as np

NB_NODE_MAX = 10000
CENTRALISATION_MAX = (NB_NODE_MAX/2)
NB_LONG_MAX = 100

NB_ESSAIS = 100
NB_FOURMIS = 500
ARRIVE = rand.randrange(2, CENTRALISATION_MAX)
DEPART = 0
print("Nid des fourmis : " + str(DEPART) +
      ". Nourriture trouvée : " + str(ARRIVE) + ". Dépot de phéromone pour optimiser le trajet retour.")
G = nx.Graph()


def init():
    for x in range(NB_NODE_MAX):
        G.add_edge(rand.randrange(0, CENTRALISATION_MAX), rand.randrange(
            0, CENTRALISATION_MAX), longueur=rand.randrange(0, NB_LONG_MAX), pheromone=1)
    # print(G.edges.data())
    nx.draw(G, with_labels=True, font_weight='bold')
    plt.show()


def deplacer(node, deplacement):
    # Permet de se déplacer entre chaque node en fonction de si il y a des phéromones sur la route
    somme_longueurs = G.degree(node, weight='longueur')
    somme_pheromones = G.degree(node, weight='pheromone')
    proabilite = []
    arrives = []
    if somme_longueurs == 0 or somme_pheromones == 0:
        return -1
    for node1, node2, data in G.edges(node, data=True):
        if deplacement.count(node2) <= 0:
            proba = (((1-(data['longueur']/somme_longueurs)) *
                      0.05) + (data['pheromone']/somme_pheromones * 0.95))
            proabilite.append(proba)
            arrives.append(node2)
    if len(arrives) == 0:
        return -1
    proabilite = np.array(proabilite)
    proabilite /= proabilite.sum()
    res = choice(arrives, size=1, p=proabilite)[0]
    return res


def pheromone(deplacement):
    # Place des phéromones sur la route de retour vers le nid.
    taille_graph = G.size(weight='longueur')
    distance = 0
    iternodes = iter(deplacement)
    precedent_node = next(iternodes)
    for n in iternodes:
        distance += G[precedent_node][n]['longueur']
        precedent_node = n

    nb_a_poser = taille_graph/distance
    iternodes = iter(deplacement)
    precedent_node = next(iternodes)
    for n in iternodes:
        G[precedent_node][n]['pheromone'] += round(nb_a_poser)
        precedent_node = n


def evaporation():
    # Probabilité que les phéromones s'évaporent sur la route.
    for node1, node2 in G.edges:
        G[node1][node2]['pheromone'] = round(G[node1]
                                              [node2]['pheromone']*0.05)


def afficher_parcours():
    # Permet d'afficher la liste des nodes traversé
    print('fini')
    chemin = [ARRIVE]
    node = ARRIVE
    nb_essai = 0
    while node != DEPART:
        maximum = 0
        nb_essai += 1
        if nb_essai > 1000:
            print('raté')
            print(chemin)
            return None
        for node1, node2, data in G.edges(node, data=True):
            if data['pheromone'] > maximum and node2 not in chemin:
                maximum = data['pheromone']
                node = node2
        chemin.append(node)
    print('parcours optimal :')
    print(chemin)


def exploration():
    # Explore l'environnement jusqu'à trouver de la nourriture en traçant la route de phéromone si ils ont trouvé la source de nourriture. 
    for i in range(NB_ESSAIS):
        for j in range(NB_FOURMIS):
            node = DEPART
            deplacement = [DEPART]
            while node != ARRIVE:
                node = deplacer(node, deplacement)
                if node == -1:
                    break
                deplacement.append(node)
            if node == ARRIVE:
                pheromone(deplacement)
        evaporation()
    afficher_parcours()


if __name__ == "__main__":
    init()
    print('fin init')
    exploration()
    print('fin du test')
